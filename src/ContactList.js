import React from 'react';
import ContactCard from './ContactCard';

function ContactList() {
    return(
        <div>
            <ContactCard 
                contact={{name:"Mr Robot", imgUrl:"https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260", phone:"+23287387783", email:"mrrobot@mail.com"}}
            />
            <ContactCard 
                contact={{name: "Mr Reese",
                imgUrl: "https://images.pexels.com/photos/458473/pexels-photo-458473.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
                phone: "+23287387783",
                email: "mrreeset@mail.com"}}
            />
            <ContactCard
                contact={{
                    name: "Mr Rainbow",
                    imgUrl: "https://images.pexels.com/photos/458665/pexels-photo-458665.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
                    phone: "+23287387783",
                    email: "rainbow@gmail.com"
                }}
                
            />
            <ContactCard
                contact={{
                name: "Mr Rotterman",  
                imgUrl: "https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",  
                phone: "+23287387783",  
                email: "rotterman@mail.com"
                }}                
            />

            <ContactCard 
                contact={{name:"Mr Robot", imgUrl:"https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260", phone:"+23287387783", email:"mrrobot@mail.com"}}
            />
        </div>
    )
}

export default ContactList;