import React from 'react';
import './Todo.css';
import TodoItem from './TodoItem';
import TodoData from './TodoData';
import sampledata from './vizdata/sampledata';


import { ResponsiveScatterPlot } from '@nivo/scatterplot';
const  nivoProps = {
    width: 900,
    height: 500,
    margin: { top: 24, right: 24, bottom: 80, left: 80 },
    symbolSize: 15,
    axisBottom: {
      format: d => `week ${d}`,
    },
    axisLeft: {
      format: d => `${d} kg`,
    },
    data: sampledata,
    legends: [
      {
        anchor: 'bottom-left',
        direction: 'row',
        translateY: 60,
        itemWidth: 130,
        itemHeight: 12,
        symbolSize: 12,
        symbolShape: 'circle',
      },
    ],
    colors: ["#ff0033", "#acff24" , "#3ee9f7"]
}

class Todo extends React.Component {
    constructor () {
        super();
        this.state = {
            // todoList: TodoData,
           
        }
        // this.handleChange = this.handleChange.bind(this);
        this.getViz = this.getViz.bind(this);
        this.handleMouseClick = this.handleMouseClick.bind(this);
    }
    // handleChange(id) {
    //     this.setState(prevState => {
    //         const updatedTodos = prevState.todoList.map(todoItem => {
    //             if(todoItem.id === id){
    //                 todoItem.completed = !todoItem.completed;
    //             }
    //             return todoItem;
    //         });

    //         return {
    //             todoList: updatedTodos
    //         }
    //     })
    // }

    handleMouseClick = point => {
        alert("CLicked point: " + point.x)
    }
    getViz() {
        return(
            <div className="viz-container">
                <ResponsiveScatterPlot
                    {...nivoProps}
                    onClick = {this.handleMouseClick}
                />
            </div>
        )
    }
    render(){
        // const todoItems = this.state.todoList.map(item => <TodoItem key = {item.id}
        //             item = {item}
        //             handleChange = {this.handleChange}
        //     />);

        return (
            <div className="todo-container">
                {/* {todoItems} */}
                {this.getViz()}
            </div>
        );
    }
}

export default Todo;