import React from 'react';
import Joke from './Joke'
import './JokeApp.css';
import JokesData from './JokesData';

function JokeApp() {
    const jokeComponents = JokesData.map(joke => <Joke key= {joke.id}
        question= {joke.question}
        punchline = {joke.punchLine}
    />
)
    return(
        <div id="joke-container">
            {jokeComponents}
        </div>
    )
}

export default JokeApp;