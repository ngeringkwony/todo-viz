import React from 'react';
import { red } from 'ansi-colors';

function TodoItem(props) {
    const styles = {}

    if(props.item.completed) {
        styles.textDecoration = "line-through"
    }
    return(
        <div className="todo-item"> 
            <input 
                onChange={(event) => {props.handleChange(props.item.id)}}
                type="checkbox" 
                checked = {props.item.completed} />

            <p style={styles}>{props.item.text}</p>
        </div>
    );
}

export default TodoItem;