import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import TodoApp from './TodoApp';
import * as serviceWorker from './serviceWorker';
import ContactList from './ContactList';
import JokeApp from './JokeApp';
import ProductApp from './ProductApp';
import StateIntro from './StateIntro';
import EventsApp from './EventsApp';

ReactDOM.render(<TodoApp />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
