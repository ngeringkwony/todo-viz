import React from 'react';
import { red } from 'ansi-colors';

function Joke(props) {
    return(
        <div id= "joke">
            <p style={{display: !props.question && "none"}}>Question: {props.question}</p>
            <p style={{color: !props.question && "red"}}>Punchline: {props.punchline}</p>
        </div>
    ) 
}

export default Joke;