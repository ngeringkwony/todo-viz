import React from "react";

// Given a stateless functional component, add state to it
// state should have a property called `isLoggedIn` which is a boolean
// (true if logged in, false if not)
// Then, give your best shot at rendering the word "in" if the user is logged in
// or "out" if the user is logged out.

class StateIntro extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: true,
            count: 2
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(previousState => {
            return {
                count: previousState.count * 2
            }
        } );
    }

    render(){
        return (
            <div>
                <h1>You are currently logged {this.state.isLoggedIn ? "In" : "Out"}</h1>
                <h2>{this.state.count}</h2>
                <button onClick = {this.handleClick}>Change Count</button>
            </div>
        )
    }
}

export default StateIntro;
