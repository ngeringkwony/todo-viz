import React from 'react';

class EventsApp extends React.Component {
    constructor () {
        super();
        this.state = {

        }
    }

    
    render() {
        function handleClick() {
            console.log("I was freakin clicked");
        }

    
        return (
            <div>
            <img onMouseOver = {() => console.log("HOVERED OVER IMAGE")} src="https://www.fillmurray.com/200/100"/>
            <br />
            <br />
            <button onClick={handleClick}>Click me</button>
        </div>
        );
    }
}

export default EventsApp;