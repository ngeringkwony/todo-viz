import React from 'react';
import Product from './Product';
import ProductData from './ProductData';

function ProductApp() {
    console.log(ProductData);
    const products = ProductData.map(product => <Product key = {product.id} 
        name = {product.name}
        description = {product.description}
        price = {product.price}
        />);
    return(
        <div id="joke-container">
            {products}
        </div>
    );
}

export default ProductApp;